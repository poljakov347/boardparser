package com.polyakov.boardparser.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class BoardStringUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoardStringUtil.class);

    /**
     Возвращает хвост строки с конца
     */
    public String getStringForEnd(String s, String separator){
        LOGGER.debug("getStringForEnd string = "+s+" separator = "+separator);
        return s.substring(s.lastIndexOf(separator)+1);

    }

    public String getStringForEnd(String s, String separator, String startPosition, String endPosition){
        LOGGER.debug("getStringForEnd string = "+s+" separator = "+separator);
        String oneString = s.substring(s.lastIndexOf(separator));
        return oneString.substring(separator.length(),oneString.indexOf(endPosition));

    }

    public String getStringForDoEnd(String s, String separator){
        LOGGER.debug("getStringForEnd string = "+s+" separator = "+separator);
        return s.substring(0,s.lastIndexOf(separator)+1);

    }

    /*
    Возвращаем начало
     */
    public String getStringForStart(String s, String separator){
        LOGGER.debug("getStringForEnd string = "+s+" separator = "+separator);
        return s.substring(s.indexOf(separator)-1);

    }

    public String getStringForStart(String s, String separator,int startPosition){
        LOGGER.debug("getStringForEnd string = "+s+" separator = "+separator);
        return s.substring(0,s.indexOf(separator,startPosition));

    }

}
