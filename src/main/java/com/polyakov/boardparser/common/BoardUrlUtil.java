package com.polyakov.boardparser.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@Service
public class BoardUrlUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoardUrlUtil.class);

    /*
    * Собираем url из параметеров
    * */
    public String getUrlConcat(Map<String, String> mapList) {
        String urlConcat = "";
        for (String key:mapList.keySet()){
            LOGGER.debug("getUrlConcat "+key+"="+mapList.get(key));
            if(urlConcat.equals("")){
               urlConcat=key+"="+mapList.get(key);
            }
            else {
                urlConcat = urlConcat + "&"+key+"="+mapList.get(key);
            }
        }
        return urlConcat;
    }

}
