package com.polyakov.boardparser.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class BoardCompare {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoardCompare.class);

    /*
    Сравниваем размер текущий и ранее записанный в БД, чтобы лишний раз не парсить
     */
    public boolean isNeedParse (String curr, String old){
        if (!curr.equals(old) ){
            return false;
        }
        else {
            return true;
        }
    }
}
