package com.polyakov.boardparser.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class BoardDateUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoardDateUtil.class);

    public String getformatYyyyMMdd (Date date){
        SimpleDateFormat formatYyyyMMdd = new SimpleDateFormat("yyyyMMdd");
        return formatYyyyMMdd.format(date.getTime());
    }

    public String getformatYyyy_MM_dd (Date date){
        SimpleDateFormat formatYyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
        return formatYyyyMMdd.format(date.getTime());
    }

    /*
    Сравниваем текущую дату с зоной аэропорта и возвращаем дату с указанием смещения в днях
     */
    ////Доработать, т.к. дата может поменяться с учетом временной зоны
    // Разделить классы на даты , строки и прочее
    public Calendar getCurrentDateAirport(String timeZone, Integer offset){
        Calendar currentDate = Calendar.getInstance();
        String currDateNoTZ = getformatYyyyMMdd(currentDate.getTime());
        Long currDateNoTZML = currentDate.getTimeInMillis();

        if (!currentDate.getTimeZone().getID().equals(timeZone)){
            currentDate.getTimeZone().setID(timeZone);
        }
        String currDateIsTZ = getformatYyyyMMdd(currentDate.getTime());

        if (!currDateIsTZ.equals(currDateNoTZ)){
            if (currDateNoTZML < currentDate.getTimeInMillis()){
                offset--;
            }
            else {
                offset++;
            }
        }

        if (offset != 0){
            currentDate.add(Calendar.DATE,offset);
        }
        LOGGER.debug("getCurrentDateAirport "+currentDate);
        return currentDate;
    }

    public Object getCurrentRealDateForTime(Calendar currDate, Integer planTime, Integer realTime){
        if (planTime == realTime){
            return  currDate;
        }
        Integer offset = 0;
        // 00:01 < 23:50
        if (planTime < realTime){
            offset--;
        }
        // 23:50 > 00:01
        else if (planTime > realTime){
            offset++;
        }
        currDate.add(Calendar.DATE,offset);
        return currDate;
    }

}
