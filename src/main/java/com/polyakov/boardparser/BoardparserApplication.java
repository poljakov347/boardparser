package com.polyakov.boardparser;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
@SpringBootApplication(scanBasePackages = {"com.polyakov"})
@EnableJpaRepositories
public class BoardparserApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoardparserApplication.class, args);
	}

}
