package com.polyakov.boardparser.task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.polyakov.boardparser.model.AirportEntity;
import com.polyakov.boardparser.rabbit.AirportMessageSender;
import com.polyakov.boardparser.repository.AirportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@EnableScheduling
@Service
public class ScheduledTasks {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledTasks.class);

    @Autowired
    AirportRepository airportRepository;

    @Autowired
    AirportMessageSender airportMessageSender;

    @Scheduled(fixedRate = 50000)
    public void airport() {
        List<AirportEntity> airportEntityList = airportRepository.getAirportEntitiesByIsEnabledIsTrueAndIsIndividualIsTrue();
        for (AirportEntity airportEntity : airportEntityList
        ) {
            LOGGER.debug("airport send start " + airportEntity.getCode());
            airportMessageSender.sendAirport(airportEntity);
        }
    }
}
