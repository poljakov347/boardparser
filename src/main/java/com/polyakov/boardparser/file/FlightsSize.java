package com.polyakov.boardparser.file;

import com.polyakov.boardparser.parser.ParserBase;
import jdk.nashorn.internal.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class FlightsSize {
    private static final Logger LOGGER = LoggerFactory.getLogger(FlightsSize.class);

    public String getFlightFile (String fileName) {
        fileName=fileName+".json";
        try {
            File file = new File(fileName);
            if (!file.exists()){
                file.createNewFile();
            }
            return String.valueOf(Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8));

        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    public void setFlightFile (String fileName, String data){
        fileName=fileName+".json";
        File file = new File(fileName);
        if (!file.exists()){
            return;
        }
        try {
            Files.write(Paths.get(fileName),data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
        }
    }

}
