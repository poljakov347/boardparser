package com.polyakov.boardparser.model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "flights", schema = "aviaguru", catalog = "")
public class FlightsEntity {
    private String flNumber;
    private String direction;
    private String originalAirport;
    private String destinationAirport;
    private String timeSchedule;
    private String timeReal;
    private Date date;
    private String status;
    private String terminal;
    private String gate;
    private String baggage;
    private String flAttr;
    private Timestamp lastUpdate;
    private String id;
    private Date dateReal;
    private String customId;
    private boolean checkIn;
    private boolean isLive;
    private int customSize;

    @Basic
    @Column(name = "fl_number")
    public String getFlNumber() {
        return flNumber;
    }

    public void setFlNumber(String flNumber) {
        this.flNumber = flNumber;
    }

    @Basic
    @Column(name = "direction")
    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Basic
    @Column(name = "original_airport")
    public String getOriginalAirport() {
        return originalAirport;
    }

    public void setOriginalAirport(String originalAirport) {
        this.originalAirport = originalAirport;
    }

    @Basic
    @Column(name = "destination_airport")
    public String getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    @Basic
    @Column(name = "time_schedule")
    public String getTimeSchedule() {
        return timeSchedule;
    }

    public void setTimeSchedule(String timeSchedule) {
        this.timeSchedule = timeSchedule;
    }

    @Basic
    @Column(name = "time_real")
    public String getTimeReal() {
        return timeReal;
    }

    public void setTimeReal(String timeReal) {
        this.timeReal = timeReal;
    }

    @Basic
    @Column(name = "date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "terminal")
    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    @Basic
    @Column(name = "gate")
    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    @Basic
    @Column(name = "baggage")
    public String getBaggage() {
        return baggage;
    }

    public void setBaggage(String baggage) {
        this.baggage = baggage;
    }

    @JoinColumn
    @Column(name = "fl_attr")
    public String getFlAttr() {
        return flAttr;
    }

    public void setFlAttr(String flAttr) {
        this.flAttr = flAttr;
    }

    @Basic
    @Column(name = "last_update")
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Id
    @Column(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "date_real")
    public Date getDateReal() {
        return dateReal;
    }

    public void setDateReal(Date dateReal) {
        this.dateReal = dateReal;
    }

    @Basic
    @Column(name = "custom_id")
    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    @Basic
    @Column(name = "check_in")
    public boolean getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(boolean checkIn) {
        this.checkIn = checkIn;
    }

    @Basic
    @Column(name = "is_live")
    public boolean getIsLive() {
        return isLive;
    }

    public void setIsLive(boolean isLive) {
        this.isLive = isLive;
    }

    @Basic
    @Column(name = "custom_size")
    public int getCustomSize() {
        return customSize;
    }

    public void setCustomSize(int customSize) {
        this.customSize = customSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightsEntity that = (FlightsEntity) o;
        return isLive == that.isLive &&
                customSize == that.customSize &&
                Objects.equals(flNumber, that.flNumber) &&
                Objects.equals(direction, that.direction) &&
                Objects.equals(originalAirport, that.originalAirport) &&
                Objects.equals(destinationAirport, that.destinationAirport) &&
                Objects.equals(timeSchedule, that.timeSchedule) &&
                Objects.equals(timeReal, that.timeReal) &&
                Objects.equals(date, that.date) &&
                Objects.equals(status, that.status) &&
                Objects.equals(terminal, that.terminal) &&
                Objects.equals(gate, that.gate) &&
                Objects.equals(baggage, that.baggage) &&
                Objects.equals(flAttr, that.flAttr) &&
                Objects.equals(lastUpdate, that.lastUpdate) &&
                Objects.equals(id, that.id) &&
                Objects.equals(dateReal, that.dateReal) &&
                Objects.equals(customId, that.customId) &&
                Objects.equals(checkIn, that.checkIn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flNumber, direction, originalAirport, destinationAirport, timeSchedule, timeReal, date, status, terminal, gate, baggage, flAttr, lastUpdate, id, dateReal, customId, checkIn, isLive, customSize);
    }
}
