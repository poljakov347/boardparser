package com.polyakov.boardparser.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "airport", schema = "aviaguru", catalog = "")
public class AirportEntity {
    private String code;
    private String cityCode;
    private String counrtyCode;
    private String timeZone;
    private String name;
    private String status;
    private String size;
    private boolean isIndividual;
    private boolean isProxy;
    private boolean isEnabled;

    @Id
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "city_code")
    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    @Basic
    @Column(name = "counrty_code")
    public String getCounrtyCode() {
        return counrtyCode;
    }

    public void setCounrtyCode(String counrtyCode) {
        this.counrtyCode = counrtyCode;
    }

    @Basic
    @Column(name = "time_zone")
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "size")
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Basic
    @Column(name = "is_individual")
    public boolean getIsIndividual() {
        return isIndividual;
    }

    public void setIsIndividual(boolean isIndividual) {
        this.isIndividual = isIndividual;
    }

    @Basic
    @Column(name = "is_proxy")
    public boolean getIsProxy() {
        return isProxy;
    }

    public void setIsProxy(boolean isProxy) {
        this.isProxy = isProxy;
    }

    @Basic
    @Column(name = "is_enabled")
    public boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    @Override
    public String toString() {
        return "AirportEntity{" +
                "code='" + code + '\'' +
                ", cityCode='" + cityCode + '\'' +
                ", counrtyCode='" + counrtyCode + '\'' +
                ", timeZone='" + timeZone + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", size=" + size +
                ", isIndividual=" + isIndividual +
                ", isProxy=" + isProxy +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AirportEntity that = (AirportEntity) o;
        return isIndividual == that.isIndividual &&
                isProxy == that.isProxy &&
                Objects.equals(code, that.code) &&
                Objects.equals(cityCode, that.cityCode) &&
                Objects.equals(counrtyCode, that.counrtyCode) &&
                Objects.equals(timeZone, that.timeZone) &&
                Objects.equals(name, that.name) &&
                Objects.equals(status, that.status) &&
                Objects.equals(size, that.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, cityCode, counrtyCode, timeZone, name, status, size, isIndividual, isProxy);
    }
}
