package com.polyakov.boardparser.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "proxy", schema = "aviaguru", catalog = "")
public class ProxyEntity {
    private String ip;
    private int port;

    @Id
    @Column(name = "ip")
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Basic
    @Column(name = "port")
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProxyEntity that = (ProxyEntity) o;
        return port == that.port &&
                Objects.equals(ip, that.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, port);
    }
}
