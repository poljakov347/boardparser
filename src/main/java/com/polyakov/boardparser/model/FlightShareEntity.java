package com.polyakov.boardparser.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "flight_share", schema = "aviaguru", catalog = "")
public class FlightShareEntity {
    private String flNumber;
    private String flightShareNumber;

    @Id
    @Column(name = "fl_number")
    public String getFlNumber() {
        return flNumber;
    }

    public void setFlNumber(String flNumber) {
        this.flNumber = flNumber;
    }

    @Basic
    @Column(name = "flight_share_number")
    public String getFlightShareNumber() {
        return flightShareNumber;
    }

    public void setFlightShareNumber(String flightShareNumber) {
        this.flightShareNumber = flightShareNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightShareEntity that = (FlightShareEntity) o;
        return Objects.equals(flNumber, that.flNumber) &&
                Objects.equals(flightShareNumber, that.flightShareNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flNumber, flightShareNumber);
    }
}
