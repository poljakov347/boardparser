package com.polyakov.boardparser.parser;

import com.polyakov.boardparser.model.AirportEntity;
import com.polyakov.boardparser.model.FlightShareEntity;
import com.polyakov.boardparser.model.FlightsEntity;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IParserBase {

    void getFlightsList (Elements elementsFl);

    void getAllFlights (AirportEntity airportEntity);

    void saveAirport (AirportEntity airportEntity);

    void saveAllFlights (List<FlightsEntity> flightsEntityList, List<FlightShareEntity> flightShareEntityList);
}
