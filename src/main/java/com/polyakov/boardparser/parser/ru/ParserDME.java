package com.polyakov.boardparser.parser.ru;

import com.polyakov.boardparser.common.*;
import com.polyakov.boardparser.file.FlightsSize;
import com.polyakov.boardparser.model.AirportEntity;
import com.polyakov.boardparser.model.FlightShareEntity;
import com.polyakov.boardparser.model.FlightsEntity;
import com.polyakov.boardparser.parser.ParserBase;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Date;
import java.util.*;

@Service
public class ParserDME extends ParserBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParserDME.class);
    private String currFligth;

    @Autowired
    BoardDateUtil boardDateUtil;

    @Autowired
    BoardStringUtil boardStringUtil;

    @Autowired
    BoardCompare boardCompare;

    @Override
    protected void setFieldsUrl() {
        DIRECTION = "direction";
        DIRECTION_ARR = "A";
        DIRECTION_DEP = "D";
        PERRIODDATE = "periodday";
        PERIOD_HOUR = "periodhour";
        PAGE = "page";
        PERRIODDATE_VALUE = new Object[]{-1, 0, 1, 2};
    }

    @Override
    public void getAllFlights(AirportEntity airportEntity) {
        LOGGER.debug("ParserDME start getAllFlights");
        setFieldsUrl();
        isProxy = airportEntity.getIsProxy();
        oldSizeFlightsList = jsonToMap(airportEntity.getSize());
        getFilesFl(airportEntity.getCode());
        getCicleParse(DIRECTION_ARR, airportEntity);
        getCicleParse(DIRECTION_DEP, airportEntity);
        setFilesFl(airportEntity.getCode());
        airportEntity.setSize(mapToJson(newSizeFlightsList));
        airportEntity.setIsEnabled(true);

        saveAllFlights(flightsEntityList,flightShareEntityList);
        saveAirport(airportEntity);
    }

    private String getFlNumber(String fNumber) {
        String flightN = boardStringUtil.getStringForStart(fNumber, " ", 4);
        if (flightN != null) {
            flightN = flightN.replaceAll(" ", "");
        }
        return flightN;
    }

    private void getCicleParse(String route, AirportEntity airportEntity) {
        LOGGER.debug("getCicleParse " + route);

        if (route.equals("A")) {
            currRoute = DirectionEnum.ARR;
        } else if (route.equals("D")) {
            currRoute = DirectionEnum.DEP;
        }

        int pageCount = 0;
        try {
            for (int i = 0; i < PERRIODDATE_VALUE.length; i++) {

                //Доработать, т.к. дата может поменяться с учетом временной зоны
                currDate = boardDateUtil.getCurrentDateAirport(airportEntity.getTimeZone(), (Integer) PERRIODDATE_VALUE[i]);

                boolean isCicle = true;

                while (isCicle) {
                    getBaseMap(route, String.valueOf(PERRIODDATE_VALUE[i]), String.valueOf(pageCount));
                    String urlConcat = getConcatUrl(mapUrl);
                    LOGGER.debug("getCicleParse before getParseToGet " + Const.DME_URL_PART1 + Const.DME_URL_PART2 + "?" + urlConcat);
                    Document document = getParseToGet(Const.DME_URL_PART1 + Const.DME_URL_PART2 + "?" + urlConcat, isProxy);
                    Elements elementsFligts = null;
                    String pageHash = null;
                    if (document != null) {
                        //Список общий
                        elementsFligts = document.getElementsByTag("li");
                        if (elementsFligts != null && elementsFligts.size() > 0) {
                            pageHash = elementsFligts.first().getElementsByTag("a").attr("href");
                            if (pageHash != null) {
                                pageHash = boardStringUtil.getStringForEnd(pageHash, "=");
                            }
                        }
                    }

                    if (document == null || elementsFligts == null || pageHash == null) {
                        pageCount = 0;
                        isCicle = false;
                        continue;
                    }

                    String oldSize = null;
                    if (oldSizeFlightsList != null && oldSizeFlightsList.get(route + pageHash) != null) {
                        oldSize = oldSizeFlightsList.get(route + pageHash);
                    }
                    String currSize = getSize(elementsFligts.text());

                    if (oldSize == null || !boardCompare.isNeedParse(currSize, oldSize)) {
                        LOGGER.debug("getFlights parsing from " + route + pageHash);
                        newSizeFlightsList.put(route + pageHash, currSize);
                    } else {
                        newSizeFlightsList.put(route + pageHash, oldSize);
                    }
                    getFlightsList(elementsFligts);
                    pageCount++;
                }
            }
        } catch (IOException e) {
            LOGGER.debug("getFlights DME " + e.getMessage());
            //e.printStackTrace();
        }
    }

    private void getBaseMap(String route, String periodDate, String page) {
        LOGGER.debug("getBaseMap " + route + " " + periodDate + " " + page);
        mapUrl.put(DIRECTION, route);
        mapUrl.put(PERRIODDATE, periodDate);
        mapUrl.put(PERIOD_HOUR, "0");
        mapUrl.put(PAGE, page);
    }


    @Override
    public void getFlightsList(Elements elementsFl) {
        LOGGER.debug("ParserDME start getFlightsList");
        for (ListIterator<Element> it = elementsFl.listIterator(); it.hasNext(); ) {
            Element element = it.next();
            String flCurHashCode = getSize(element.text());
            String flOldHashCode = null;

            String linkDetailUrl = element.getElementsByTag("a").attr("href");
            String id = boardStringUtil.getStringForEnd(linkDetailUrl, "=");
            String flNumber;
            flNumber = element.select("div.flightNumber").text();

            //Копируем старые данные с размером
            if (!oldSizeFlight.isEmpty() && oldSizeFlight.get(id) != null) {
                flOldHashCode = oldSizeFlight.get(id);
                if (flOldHashCode != null && boardCompare.isNeedParse(flOldHashCode, flCurHashCode)) {
                    newSizeFlight.put(id, flOldHashCode);
                    continue;
                }
            }

            if (flNumber.indexOf("shared with") > 0) {
                //Собираем связанные рейсы
                String currShareFLNumber = boardStringUtil.getStringForEnd(flNumber, "shared with", "shared with", ")");
                if (currShareFLNumber == null) {
                    newSizeFlight.put(id, flCurHashCode);
                    continue;
                }
                currShareFLNumber = currShareFLNumber.replaceAll(" ", "");
                flNumber = getFlNumber(flNumber);
                FlightShareEntity flightShareEntity = new FlightShareEntity();
                flightShareEntity.setFlNumber(currShareFLNumber);
                flightShareEntity.setFlightShareNumber(flNumber);
                flightShareEntityList.add(flightShareEntity);
                newSizeFlight.put(id, flCurHashCode);
                continue;
            }
            flightId.add(id);
            newSizeFlight.put(id, flCurHashCode);
            FlightsEntity flightsEntity = new FlightsEntity();
            flightsEntity.setCustomId(id);
            String dateSt = boardDateUtil.getformatYyyyMMdd(currDate.getTime());
            String dateStD = boardDateUtil.getformatYyyy_MM_dd(currDate.getTime());

            //Сверить дату из детайл, там может вылет в преддыдущем дне а прилет уже в текущем

            flNumber = getFlNumber(flNumber);

            flightsEntity.setFlNumber(dateSt + flNumber);
            flightsEntity.setTimeSchedule(element.select("div.plannedDateFlight.ui-inline").text());
            flightsEntity.setStatus(element.select("div.flightstatus").text());
            flightsEntity.setDirection(String.valueOf(currRoute));
            flightsEntity.setDate(Date.valueOf(dateStD));

            if (!element.select("div.actualDateFlight.ui-inline").text().equals("")) {
                flightsEntity.setTimeReal(element.select("div.actualDateFlight.ui-inline").text());
            }

            Integer planTime = Integer.valueOf(flightsEntity.getTimeSchedule().replaceAll(":", ""));
            if (flightsEntity.getTimeReal() != null) {
                Integer realTime = Integer.valueOf(flightsEntity.getTimeReal().replaceAll(":", ""));
                Calendar dateReal = (Calendar) boardDateUtil.getCurrentRealDateForTime(currDate, planTime, realTime);
                String dateRealStD = boardDateUtil.getformatYyyy_MM_dd(dateReal.getTime());
                flightsEntity.setDateReal(Date.valueOf(dateRealStD));
            }

            currFligth = flightsEntity.getFlNumber() + flightsEntity.getDirection();
            flightsEntity.setId(currFligth);

            getFlDetail(Const.DME_URL_PART1 + linkDetailUrl, flightsEntity, id);

            flightsEntityList.add(flightsEntity);


        }
    }

    private void getFlDetail(String url, FlightsEntity flightsEntity, String flightID) {
        try {
            Document document = getParseToGet(url, isProxy);
            Elements elementHead = document.select("#headerMain");
            Elements elementDetail = document.select("#contentMain");

            if (elementDetail.size() < 1 && elementHead.size() < 1) {
                return;
            }
            String timeFly = elementHead.select("#headerMain > div > div:nth-child(8) > div").text();
            if (timeFly != null) {
                timeFly = timeFly.replaceAll("h.",":").replaceAll("mins","").replaceAll(" 0","00");
            }

            String boardingModel = "";
            String terminal = "";
            String gate = "";
            String baggageStr = "";
            String baggage = "";
            String registerWindows = "";
            flightsEntity.setDestinationAirport(elementHead.select("#headerMain > div > div:nth-child(6)").text());
            flightsEntity.setOriginalAirport(elementHead.select("#headerMain > div > div:nth-child(4)").text());

            if (currRoute.equals(DirectionEnum.ARR)) {

                boardingModel = elementDetail.select("#contentMain > ul > li:nth-child(7) > div > div.ui-block-b").text();
                terminal = elementDetail.select("#contentMain > ul > li:nth-child(10) > div > div.ui-block-b").text();
                baggageStr = elementDetail.select("#contentMain > ul > li:nth-child(8) > div > div.ui-block-b").text();
                baggage = elementDetail.select("#contentMain > ul > li:nth-child(9) > div > div.ui-block-b").text();

                //Пробуем получить номер ленты
                if (baggageStr.equals("Baggage issue") || baggage == null) {
                    url = url.replaceAll("https://m.", "https://");
                    Document documentDet = getParseToGet(url, isProxy);
                    if (documentDet != null) {
                        String baggageTemp = documentDet.select("body > div > div:nth-child(5) > div.detail-page-font > div:nth-child(5) > div.margin-bottom-10.pullleft_70").text();
                        if (baggageTemp != null) {
                            baggage = boardStringUtil.getStringForEnd(baggageTemp, " ");
                        }
                    }

                    if (baggage == null) {
                        newSizeFlight.put(flightID, "no baggage");
                    }
                }
            } else if (currRoute.equals(DirectionEnum.DEP)) {
                boardingModel = elementDetail.select("#contentMain > ul > li:nth-child(7) > div > div.ui-block-b").text();
                terminal = elementDetail.select("#contentMain > ul > li:nth-child(12) > div > div.ui-block-b").text();
                gate = elementDetail.select("#contentMain > ul > li:nth-child(11) > div > div.ui-block-b").text();
                registerWindows = elementDetail.select("#contentMain > ul > li:nth-child(9) > div > div.ui-block-b").text();
            }

            flightsEntity.setTerminal(terminal);
            flightsEntity.setGate(gate);
            flightsEntity.setBaggage(baggage);

            //Детали рейса
            Map<FlightAttributesEnum, String> flightAttributes = new HashMap<>();

            if (timeFly != null && timeFly.length() > 0) {
                flightAttributes.put(FlightAttributesEnum.TIMEFLY, timeFly);
            }

            if (boardingModel != null && boardingModel.length() > 0) {
                flightAttributes.put(FlightAttributesEnum.BOARDINGMODEL, boardingModel);
            }

            if (registerWindows != null && registerWindows.length() > 0) {
                flightAttributes.put(FlightAttributesEnum.REGISTERWINDOW, registerWindows);
            }

            if (!flightAttributes.isEmpty()) {
                flightsEntity.setFlAttr(mapToJson(flightAttributes));
            }

        } catch (IOException e) {
            LOGGER.error("getFlDetail DME " + e.getMessage());
            e.printStackTrace();
        }

    }

}
