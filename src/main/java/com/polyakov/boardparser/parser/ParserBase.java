package com.polyakov.boardparser.parser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.polyakov.boardparser.common.*;
import com.polyakov.boardparser.file.FlightsSize;
import com.polyakov.boardparser.model.AirportEntity;
import com.polyakov.boardparser.model.FlightShareEntity;
import com.polyakov.boardparser.model.FlightsEntity;
import com.polyakov.boardparser.model.ProxyEntity;
import com.polyakov.boardparser.repository.AirportRepository;
import com.polyakov.boardparser.repository.FlightRepository;
import com.polyakov.boardparser.repository.FlightShareRepository;
import com.polyakov.boardparser.repository.ProxyRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

@Service
public class ParserBase implements IParserBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(ParserBase.class);

    //Поля для составления URL
    protected String DIRECTION;
    protected String DIRECTION_DEP;
    protected String DIRECTION_ARR;
    protected String PERRIODDATE;
    protected String PERIOD_HOUR;
    protected String PAGE;
    protected boolean isProxy;

    //Периоды рейсов с по желательно делать в int
    protected Object[] PERRIODDATE_VALUE;

    //URL составления пути для парсинга
    protected Map<String, String> mapUrl = new HashMap<>();

    //Новый список рейсов
    protected List<FlightsEntity> flightsEntityList = new ArrayList<>();
    //Новый список совмещенных рейсов
    protected List<FlightShareEntity> flightShareEntityList = new ArrayList<>();

    //ИД рейсов уникальны
    protected Set<String> flightId = new HashSet<>();

    //Size flights from DB
    protected Map<String, String> oldSizeFlightsList = new HashMap<>();
    //Size flights
    protected Map<String, String> newSizeFlightsList = new HashMap<>();
    //Размеры данных по рейсу
    protected Map<String,String> newSizeFlight = new HashMap<>();
    //Список рейсов с размером из файла
    protected Map<String,String> oldSizeFlight = new HashMap<>();

    //Дата текущая, за которую идет парсинг рейсов
    protected Calendar currDate;
    //Направление рейсов вылет\прилет
    protected DirectionEnum currRoute;

    @Autowired
    ProxyRepository proxyRepository;

    @Autowired
    AirportRepository airportRepository;

    @Autowired
    FlightRepository flightRepository;

    @Autowired
    FlightShareRepository flightShareRepository;

    @Autowired
    BoardUrlUtil boardUrlUtil;

    @Autowired
    FlightsSize flightsSize;

    protected void setFieldsUrl(){
    }

    protected void getFilesFl(String fName){
        String fBody = flightsSize.getFlightFile(fName);
        if (fBody.length() > 2) {
            //Убираем квадратную скобюку
            fBody = fBody.substring(1,fBody.length()-1);
            oldSizeFlight.putAll(jsonToMap(fBody));
        }
    }

    protected void setFilesFl(String fName){
        String fBody = mapToJson(newSizeFlight);
        if (!fBody.isEmpty()){
            flightsSize.setFlightFile(fName,fBody);
        }
    }

    protected String getSize (String s){
        if (s == null){
            return null;
        }
        return  DigestUtils.md5DigestAsHex(s.getBytes());
    }

    /*
    * Преобразуем json in MAP
     */
    public Map jsonToMap(String json) {
        if (json == null){
            return null;
        }
        try {
            return new ObjectMapper().readValue(json, HashMap.class);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /*
     * Преобразуем  MAP in json
     */
    public String mapToJson(Map<?,?> map) {
        try {
            return new ObjectMapper().writeValueAsString(map);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public Map<String,String> getHeaders(){
        Map<String,String> map = new HashMap<>();
        map.put(Const.HEADER_CONTENT_LANG,Const.HEADER_CONTENT_LANG_VALUE_EN);
        map.put(Const.HEADER_ACCEPT_ENCODING,Const.HEADER_ACCEPT_ENCODING_VALUE);
        map.put(Const.HEADER_CONTENT_TYPE,Const.HEADER_CONTENT_TYPE_VALUE);
        return map;
    }

    @Override
    public void getFlightsList(Elements elementsFl) {

    }

    @Override
    public void saveAllFlights(List<FlightsEntity> flightsEntityList, List<FlightShareEntity> flightShareEntityList) {
        flightRepository.saveAll(flightsEntityList);
        flightShareRepository.saveAll(flightShareEntityList);
    }

    @Override
    public void getAllFlights(AirportEntity airportEntity) {
        setFieldsUrl();
    }

    @Override
    public void saveAirport(AirportEntity airportEntity) {
        airportRepository.save(airportEntity);
    }

    public String getConcatUrl(Map<String, String> mapList){
        return boardUrlUtil.getUrlConcat(mapList);
    }

    public Document getParseToGet(String url, boolean isProxy) throws IOException {
        Document doc;
        if (isProxy) {
            ProxyEntity proxyEntity = proxyRepository.findByRand();
            LOGGER.debug("getParseToGet proxy "+proxyEntity.getIp());
            doc = Jsoup //
                    .connect(url) //
                    .proxy(proxyEntity.getIp(), proxyEntity.getPort()) // sets a HTTP proxy
                    .userAgent(Const.USER_AGENT) //
                    .headers(getHeaders())
                    .get();
        }
        else {
            LOGGER.debug("getParseToGet no proxy");
            doc = Jsoup //
                    .connect(url) //
                    .userAgent(Const.USER_AGENT) //
                    .headers(getHeaders())
                    .get();
        }
        return doc;
    }

    public Document getParseToPost(String url, boolean isProxy, Map<String,String> map) throws IOException {
        Document doc;
        if (isProxy) {
            ProxyEntity proxyEntity = proxyRepository.findByRand();
            LOGGER.debug("getParseToGet proxy "+proxyEntity.getIp());
            doc = Jsoup //
                    .connect(url) //
                    .proxy(proxyEntity.getIp(), proxyEntity.getPort()) // sets a HTTP proxy
                    .userAgent(Const.USER_AGENT) //
                    .headers(getHeaders())
                    .data(map)
                    .post();
        }
        else {
            LOGGER.debug("getParseToGet no proxy");
            doc = Jsoup //
                    .connect(url) //
                    .userAgent(Const.USER_AGENT) //
                    .headers(getHeaders())
                    .data(map)
                    .post();
        }
        return doc;
    }
}
