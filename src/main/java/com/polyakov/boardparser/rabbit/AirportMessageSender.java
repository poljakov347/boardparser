package com.polyakov.boardparser.rabbit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.polyakov.boardparser.config.RabbitConfig;
import com.polyakov.boardparser.model.AirportEntity;
import com.polyakov.boardparser.repository.AirportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AirportMessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(AirportMessageSender.class);

   /* @Autowired
    private AmqpTemplate rabbitTemplate;*/
    private final RabbitTemplate rabbitTemplate;
    @Autowired
    public AirportMessageSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Autowired
    AirportRepository airportRepository;

    public void sendAirport(AirportEntity airportEntity)  {
        LOGGER.debug("Send airportEntity"+ airportEntity.toString());
        airportEntity.setIsEnabled(false);
        airportRepository.save(airportEntity);
        this.rabbitTemplate.convertAndSend(RabbitConfig.QUEUE_AIRPORTS, airportEntity);
    }

}
