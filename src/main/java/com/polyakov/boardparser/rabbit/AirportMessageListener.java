package com.polyakov.boardparser.rabbit;

import com.polyakov.boardparser.config.RabbitConfig;
import com.polyakov.boardparser.model.AirportEntity;
import com.polyakov.boardparser.parser.IParserBase;
import com.polyakov.boardparser.parser.ParserBase;
import com.polyakov.boardparser.parser.ru.ParserDME;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AirportMessageListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(AirportMessageListener.class);

    @Autowired
    ParserDME parserDME;

    @RabbitListener(queues = RabbitConfig.QUEUE_AIRPORTS)
    public void process(AirportEntity airportEntity){
        LOGGER.debug("Received airport"+airportEntity.toString()+" thread"+Thread.currentThread());
        if (airportEntity== null ){
            return;
        }
        getClassParser(airportEntity);
    }

    private void getClassParser(AirportEntity airportEntity){
        LOGGER.debug("getClassParser start "+airportEntity.getCode());

        if (airportEntity.getCode().equals("DME")){
            parserDME.getAllFlights(airportEntity);
        }
        else {
            return;
        }
    }


}
