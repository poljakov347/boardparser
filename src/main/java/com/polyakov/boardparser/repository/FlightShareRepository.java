package com.polyakov.boardparser.repository;

import com.polyakov.boardparser.model.FlightShareEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightShareRepository extends CrudRepository <FlightShareEntity, String> {
}
