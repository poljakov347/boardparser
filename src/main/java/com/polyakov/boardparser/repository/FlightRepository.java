package com.polyakov.boardparser.repository;

import com.polyakov.boardparser.model.FlightsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightRepository extends CrudRepository <FlightsEntity,String> {
}
