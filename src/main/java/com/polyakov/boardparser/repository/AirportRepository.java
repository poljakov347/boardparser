package com.polyakov.boardparser.repository;

import com.polyakov.boardparser.model.AirportEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportRepository extends CrudRepository<AirportEntity,String> {

    List<AirportEntity> getAirportEntitiesByIsIndividualIsTrue();

    List<AirportEntity> getAirportEntitiesByIsEnabledIsTrueAndIsIndividualIsTrue();

    //@Query(value = "update airport set is_enabled = false where code =", nativeQuery = true)
}
