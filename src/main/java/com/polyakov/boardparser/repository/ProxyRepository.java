package com.polyakov.boardparser.repository;

import com.polyakov.boardparser.model.ProxyEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProxyRepository extends CrudRepository<ProxyEntity,String> {

    @Query(value = "select * from proxy p  order by rand() limit 1", nativeQuery = true)
    ProxyEntity findByRand();
}
